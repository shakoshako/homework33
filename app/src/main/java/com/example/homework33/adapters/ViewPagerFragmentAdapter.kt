package com.example.homework33.adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.homework33.fragments.FirstFragment
import com.example.homework33.fragments.SecondFragment
import com.example.homework33.fragments.ThirdFragment

class ViewPagerFragmentAdapter(activity: AppCompatActivity): FragmentStateAdapter(activity) {

    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        if (position == 1) {
            return FirstFragment()
        } else if (position == 2) {
            return SecondFragment()
        } else {
            return ThirdFragment()
        }

    }
}